import { test } from 'qunit';
import moduleForAcceptance from 'ladder-client/tests/helpers/module-for-acceptance';
import nav from 'ladder-client/tests/helpers/nav-helper';

moduleForAcceptance('Acceptance | list results');

function readText(node, selector) {
  return $(node).find(selector).text().trim();
}

function listedResults() {
  return find('.result-list__result').toArray().map(r => {
    return {
      winner: readText(r, '.result-list__winner'),
      loser: readText(r, '.result-list__loser'),
      transfer: readText(r, '.result-list__transfer')*1,
    };
  });
}

test("List results", function(assert) {
  const p1 = server.create('player');
  const p2 = server.create('player');
  const r1 = server.create('result', {winner: p1, loser: p2, createdAt: '2015-10-01' });
  const r2 = server.create('result', {winner: p2, loser: p1, createdAt: '2015-09-01' });
  const r0 = server.create('result', {winner: p1, loser: p2, createdAt: '2015-11-01' });

  visit('/');
  nav.visit('results.index');
  andThen(function() {
    const results = listedResults();
    assert.equal(results[0].winner, r0.winner.name);
    assert.equal(results[0].loser, r0.loser.name);
    assert.equal(results[0].transfer, r0.transfer);

    assert.equal(results[1].winner, r1.winner.name);
    assert.equal(results[1].loser, r1.loser.name);
    assert.equal(results[1].transfer, r1.transfer);

    assert.equal(results[2].winner, r2.winner.name);
    assert.equal(results[2].loser, r2.loser.name);
    assert.equal(results[2].transfer, r2.transfer);
  });
});
