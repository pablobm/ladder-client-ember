import { test } from 'qunit';
import moduleForAcceptance from 'ladder-client/tests/helpers/module-for-acceptance';
import nav from '../helpers/nav-helper';

function candidateSelector(which, name) {
  return `.result-candidates-${which} .result-candidates-player:contains(${name})`;
}

function chooseCandidate(which, name) {
  click(candidateSelector(which, name));
}

function chooseLoser(name) {
  chooseCandidate('loser', name);
}

function chooseWinner(name) {
  chooseCandidate('winner', name);
}

function isLoserAvailable(name) {
  return find(candidateSelector('loser', name)).length === 1;
}

function isLoserMarkedAsSelected(name) {
  return find(candidateSelector('loser', name)).is('.sliding-selector-selected');
}

function isWinnerMarkedAsSelected(name) {
  return find(candidateSelector('winner', name)).is('.sliding-selector-selected');
}

function map(array, callback) {
  return Array.prototype.map.call(array, callback);
}

function readText(node, selector) {
  return $(node).find(selector).text().trim();
}

function listedRanks() {
  return map(find('.ladder-rankings-player'), rn => {
    return {
      rank:  readText(rn, '.ladder-rankings-rank')*1,
      name:  readText(rn, '.ladder-rankings-name'),
      score: readText(rn, '.ladder-rankings-score')*1,
    };
  });
}

function listedResultsCount() {
  return find('.past-result').length;
}

moduleForAcceptance('Acceptance | post results');

test("Post a result", function(assert) {
  const p1 = server.create('player');
  const p2 = server.create('player');

  visit('/');
  nav.visit('results.new');
  andThen(function() {
    assert.ok(isLoserAvailable(p1.name), `Expected "${p1.name}" to be available as loser`);
  });

  chooseWinner(p1.name);
  andThen(function() {
    assert.ok(isWinnerMarkedAsSelected(p1.name), `Expected "${p1.name}" to be marked as selected`);
    assert.ok(!isLoserAvailable(p1.name), `Expected "${p1.name}" NOT to be available as loser`);
  });

  chooseLoser(p2.name);
  andThen(function() {
    assert.ok(isLoserMarkedAsSelected(p2.name), `Expected "${p2.name}" to be marked as selected`);
  });

  click('.btn-submit');
  andThen(function() {
    assert.equal(currentRouteName(), 'root.show');
    const [r1, r2] = listedRanks();
    assert.equal(r1.name, p1.name);
    assert.equal(r1.rank, 1);
    assert.ok(r1.score > p1.score, `Expected the winner's score to have updated (went from ${p1.score} to ${r1.score})`);

    assert.equal(r2.name, p2.name);
    assert.equal(r2.rank, 2);
    assert.ok(r2.score < p2.score, `Expected the winner's score to have updated (went from ${p2.score} to ${r2.score})`);

    const flash = find('.result-announcement');
    const transfer = p2.score - r2.score;
    assert.equal(flash.find('.transfer').text()*1, transfer);
    assert.equal(flash.find('.winner').text(), p1.name);
    assert.equal(flash.find('.loser').text(), p2.name);
  });
});

test("Cancel posting a result", function(assert) {
  visit('/');
  nav.visit('results.new');
  nav.visit('root.show');
  nav.visit('results.index');

  andThen(function() {
    assert.deepEqual(listedResultsCount(), 0);
  });
});
