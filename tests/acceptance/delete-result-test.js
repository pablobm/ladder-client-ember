import { test } from 'qunit';
import moduleForAcceptance from 'ladder-client/tests/helpers/module-for-acceptance';
import nav from 'ladder-client/tests/helpers/nav-helper';

moduleForAcceptance('Acceptance | delete result');

test("Delete an existing result", function(assert) {
  let p1 = server.create('player', { score: 1000 });
  let p2 = server.create('player', { score: 1100 });
  server.create('player', { score: 1050 });

  server.create('result', {winner: p1, loser: p2});
  let r2 = server.create('result', {winner: p1, loser: p2});
  server.create('result', {winner: p1, loser: p2});

  nav.root();
  nav.visit('results.index');

  andThen(function() {
    assert.ok(deleteResultBtn().length === 0, "Confirm button should be hidden");
    assert.ok(closeResultBtn().length === 0, "Close button should be hidden");

    clickToOpenResult(r2);
  });
  andThen(function() {
    changeScoresBy(-25);
  });
  andThen(function() {
    assert.ok(deleteResultBtn().length === 1, "Confirm button should be visible");
    assert.ok(closeResultBtn().length === 1, "Close button should be visible");

    deleteResultBtn().click();
  });
  andThen(function() {
    assert.equal(latestDeleteRequest().url, `/results/${r2.id}`);
    assert.equal(currentRouteName(), 'root.show');
    assert.equal(currentFlashMessage(), "The result was deleted successfully");

    let scores = listedPlayers().map(p => p.score);
    assert.deepEqual(scores, [1075, 1025, 975]);
  });
});

test("Cancel deletion", function(assert) {
  let p1 = server.create('player');
  let p2 = server.create('player');
  server.create('result', {winner: p1, loser: p2});
  let r2 = server.create('result', {winner: p1, loser: p2});
  server.create('result', {winner: p1, loser: p2});

  nav.root();
  nav.visit('results.index');

  andThen(function() {
    clickToOpenResult(r2);
    closeResultBtn().click();
  });
  andThen(function() {
    assert.ok(deleteResultBtn().length === 0, "Confirm button should be hidden");
    assert.ok(closeResultBtn().length === 0, "Close button should be hidden");

    nav.reload();
  });

  andThen(function() {
    assert.ok(!latestDeleteRequest(), "The API should not have been told to delete the result");
    assert.equal(listedResults().length, 3);
  });
});

function deleteResultBtn() {
  return $('.btn-delete-result');
}

function changeScoresBy() {
  server.db.players.forEach((p) => {
    p.score -= 25;
    server.db.players.update(p.id, p);
  });
}

function closeResultBtn() {
  return $('.btn-close-result');
}

function clickToOpenResult(result) {
  $(`[data-result-id=${result.id}] .app-icon-more`).click();
}

function currentFlashMessage() {
  return $('.notice-message-announcement').text().trim();
}

function sentRequests() {
  return server.pretender.handledRequests;
}

function latestDeleteRequest() {
  return sentRequests().find(r => r.method === 'DELETE');
}

function listedPlayers() {
  return find('.ladder-rankings-player').toArray().map(el => {
    let p = $(el);
    return {
      id: p.data('player-id'),
      name: p.find('.ladder-rankings-name').text(),
      score: p.find('.ladder-rankings-score').text() * 1,
    };
  });
}

function listedResults() {
  return find('.result-list__result');
}

