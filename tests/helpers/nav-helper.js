export default {
  open() {
    click('.bar-nav .modal-opener');
  },

  reload() {
    visit(currentURL());
  },

  root() {
    visit('/');
  },

  visit(routeName) {
    const selector = `[data-dst="${routeName}"]`;
    click(selector);
    andThen(function() {
    });
  },
};
