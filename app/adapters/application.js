import DS from 'ember-data';
import config from 'ladder-client/config/environment';

export default DS.RESTAdapter.extend({
  host: config.APP.apiHost
});
