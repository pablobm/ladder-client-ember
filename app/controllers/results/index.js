import Ember from 'ember';

export default Ember.Controller.extend({
  announcer: Ember.inject.service('result-announcer'),

  actions: {
    deleteResult(result) {
      return result.destroyRecord().then(() => {
        this.get('announcer').announceMessage('The result was deleted successfully');
      }).catch(() => {
        this.get('announcer').announceMessage('Something went wrong :-(', 'error');
      }).finally(() => {
        this.store.unloadAll('player');
        this.transitionToRoute('root.show');
      });
    },
  },
});
