import Ember from 'ember';

function playersWithRank(players) {
  let lastScore = null;
  let lastRank = 0;
  return players.sortBy('score').reverse().map((player, i) => {
    const curScore = player.get('score');
    lastRank = curScore === lastScore ? lastRank : i+1;
    player.set('rank', lastRank);
    lastScore = curScore;
    return player;
  });
}

export default Ember.Controller.extend({
  rankedPlayers: Ember.computed('model.@each.score', function() {
    return playersWithRank(this.get('model'));
  }),
});
