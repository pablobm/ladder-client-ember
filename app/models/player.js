import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  score: DS.attr('number'),

  wins: DS.hasMany('results', {inverse: 'winner'}),
  loses: DS.hasMany('results', {inverse: 'loser'}),
});
