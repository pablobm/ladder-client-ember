export default function(){
  const ROUTES = [
    'root',
    'players',
    'results',
    'results.index',
    'results.new',
  ];

  function compareRoutes(r1, r2) {
    const i = ROUTES.indexOf(r1);
    const j = ROUTES.indexOf(r2);
    return j - i;
  }

  function fromMatcher(fromRoute, toRoute) {
    return compareRoutes(fromRoute, toRoute) > 0;
  }

  function toMatcher(toRoute, fromRoute) {
    return compareRoutes(fromRoute, toRoute) > 0;
  }

  this.transition(
    this.fromRoute(fromMatcher),
    this.toRoute(toMatcher),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.hasClass('expand-collapse'),
    this.toValue(true),
    this.use('fade', {duration: 1000})
  );
}
