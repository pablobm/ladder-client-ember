import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'nav-panel-header',
  back: false,
});
