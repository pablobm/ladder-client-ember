import Ember from 'ember';

export default Ember.Component.extend({
  results: [],
  openResult: null,
  vanishingResult: null,

  resultsSorting: ['createdAt:desc'],
  orderedResults: Ember.computed.sort('results', 'resultsSorting'),

  actions: {
    openResult(result) {
      this.set('openResult', result);
    },

    closeResult() {
      this.set('openResult', null);
    },

    deleteResult(result) {
      this.set('vanishingResult', result);
      this.attrs.deleteResult(result)
        .then(() => this.set('vanishingResult', null));
    },
  }
});
