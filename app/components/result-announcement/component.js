import Ember from 'ember';

export default Ember.Component.extend({
  announcer: Ember.inject.service('result-announcer'),

  isResultPresent: Ember.computed.bool('announcer.latestResult'),
  isMessagePresent: Ember.computed.bool('announcer.message'),

  result: Ember.computed.alias('announcer.latestResult'),
  message: Ember.computed.alias('announcer.message'),
  type: Ember.computed.alias('announcer.type'),
  winnerName: Ember.computed.alias('result.winner.name'),
  loserName: Ember.computed.alias('result.loser.name'),
  transfer: Ember.computed.alias('result.transfer'),
});
