import Ember from 'ember';

export default Ember.Component.extend({
  selected: false,

  classNameBindings: ['selected:sliding-selector-selected'],
});
