import Ember from 'ember';

export default Ember.Component.extend({
  options: [],
  label: 'label',
  code: 'code',
  eachTagName: 'div',
  eachClassName: '',

  selectedCode: null,

  _options: Ember.computed('options', 'selectedCode', function() {
    const selectedCode = this.get('selectedCode');
    return Ember.A(this.get('options').map(o => {
      const code = o.get(this.get('code'));
      const label = o.get(this.get('label'));
      return Ember.Object.create({
        selected: selectedCode === code,
        label: label,
        code: code,
      });
    }));
  }),

  actions: {
    updateSelection(code) {
      this.set('selectedCode', code);
      this.get('on-selection-change')(code);
    }
  },
});
