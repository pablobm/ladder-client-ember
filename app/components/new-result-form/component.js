import Ember from 'ember';
import buildResizeHandler from 'ladder-client/lib/build-resize-handler';
const $ = Ember.$;

export default Ember.Component.extend({
  didInsertElement() {
    const resizeHandler = buildResizeHandler(this.element, {
      base: '.result-form',
      flexibles: '.result-form-pickers',
      container: '.nav-panel-content',
    });
    this.adjustVBox = function() {
      Ember.run('afterRender', resizeHandler);
    };
    $(window).on('resize', this.adjustVBox);
  },

  didRender() {
    this.adjustVBox();
  },

  willDestroyElement() {
    $(window).off('resize', this.adjustVBox);
  },

  result: null,

  store: Ember.inject.service(),

  unsortedPlayers: Ember.computed(function() {
    return this.get('store').findAll('player');
  }),

  playerSorting: ['name'],
  players: Ember.computed.sort('unsortedPlayers', 'playerSorting'),

  winnerCandidates: Ember.computed.alias('players'),
  loserCandidates: Ember.computed('players', 'winnerId', function() {
    return this.get('players').filter(player => player.get('id') !== this.get('winnerId'));
  }),

  winnerId: null,
  loserId: null,

  isInvalid: Ember.computed('winnerId', 'loserId', function() {
    return !this.get('winnerId') || !this.get('loserId');
  }),

  loserNotSameAsWinner: Ember.observer('winnerId', function() {
    const wid = this.get('winnerId');
    const lid = this.get('loserId');
    if (wid && lid && wid === lid) {
      this.set('loserId', null);
    }
  }),

  actions: {
    save() {
      const find = id => {
        return this.get('store').peekRecord('player', id);
      };
      const w = find(this.get('winnerId'));
      const l = find(this.get('loserId'));
      Ember.RSVP.all([w, l])
        .then(([winner, loser]) => {
          this.get('result').set('winner', winner);
          this.get('result').set('loser', loser);
          this.sendAction('save');
        });
    }
  },
});
