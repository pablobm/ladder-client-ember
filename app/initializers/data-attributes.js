import Ember from 'ember';

export default {
  name: 'data-attributes',

  initialize() {
    Ember.LinkComponent.reopen({
      attributeBindings: ['data-dst']
    });
  },
};
