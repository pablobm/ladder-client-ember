import Mirage from 'ember-cli-mirage';

export default function() {

  this.get('/players', (db) => {
    return {
      data: db.players.map(attrs => ({
        type: 'players',
        id: attrs.id,
        attributes: attrs,
      })),
    };
  });

  this.post('/players', (db, request) => {
    const req = JSON.parse(request.requestBody);
    const record = db.players.insert(req.data.attributes);
    record.score = 800;
    return {
      data: {
        type: 'players',
        id: record.id,
        attributes: record,
      }
    };
  });

  this.get('/results', (db) => {
    return {
      data: db.results.map(attrs => ({
        type: 'results',
        id: attrs.id,
        attributes: {
          id: attrs.id,
          transfer: attrs.transfer,
          created_at: attrs.createdAt,
        },
        relationships: {
          winner: {
            data: {
              id: attrs.winner.id,
              type: 'players',
            },
          },
          loser: {
            data: {
              id: attrs.loser.id,
              type: 'players',
            },
          },
        }
      })),
      included: db.players.map(attrs => ({
        id: attrs.id,
        type: 'players',
        attributes: attrs,
      })),
    };
  });

  this.post('/results', (db, request) => {
    const req = JSON.parse(request.requestBody);
    const record = db.players.insert(req.data.attributes);
    const rels = req.data.relationships;
    const winner = db.players.find(rels.winner.data.id);
    const loser = db.players.find(rels.loser.data.id);
    winner.score += 10;
    loser.score -= 10;
    db.players.update(winner);
    db.players.update(loser);
    return {
      data: {
        id: record.id,
        type: 'results',
        attributes: {
          id: record.id,
          transfer: 10,
        },
        relationships: {
          winner: {
            data: {
              id: winner.id,
              type: 'players',
            },
          },
          loser: {
            data: {
              id: loser.id,
              type: 'players',
            },
          },
        },
      },
      included: [
        {
          id: winner.id,
          type: 'players',
          attributes: winner,
        }, {
          id: loser.id,
          type: 'players',
          attributes: loser,
        }
      ],
    };
  });

  this.delete('/results/:id', (db, request) => {
    db.results.remove(request.params.id);
    return new Mirage.Response(204, {}, {});
  });
}
