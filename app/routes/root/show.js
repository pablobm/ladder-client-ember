import Ember from 'ember';

export default Ember.Route.extend({
  announcer: Ember.inject.service('result-announcer'),

  model() {
    return this.store.findAll('player');
  },

  actions: {
    willTransition() {
      this.get('announcer').reset();
    },
  }
});
