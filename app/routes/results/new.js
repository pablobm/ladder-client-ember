import Ember from 'ember';

export default Ember.Route.extend({
  announcer: Ember.inject.service('result-announcer'),

  model() {
    return this.store.createRecord('result');
  },

  actions: {
    save() {
      const record = this.modelFor('results.new');
      record.save()
        .then(() => {
          this.get('announcer').announceResult(record);
          this.transitionTo('root.show');
        })
        .catch(error => {
          console.error("Error saving result", error);
        });
    },

    willTransition() {
      const record = this.controllerFor('results.new').get('model');
      if (record.get('isNew')) {
        return record.destroyRecord();
      }
    },
  },
});
