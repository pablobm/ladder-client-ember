import Ember from 'ember';

export default Ember.Service.extend({
  latestResult: null,
  message: null,
  type: 'notice',

  announceResult: function(result) {
    this.reset();
    this.set('latestResult', result);
  },

  announceMessage: function(message, type = 'notice') {
    this.reset();
    this.set('type', type);
    this.set('message', message);
  },

  reset: function() {
    this.set('latestResult', null);
    this.set('message', null);
    this.set('type', null);
  },

  isSet: Ember.computed.bool('latestResult'),
});
