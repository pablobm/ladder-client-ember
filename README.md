# Ladder Client (powered by Ember.js)

A frontend for the Thoughtbot London (nee New Bamboo) Table Tennis Ladder. A running version can be accessed at http://ladder.new-bamboo.co.uk.

The actual ladder is implemented as an API, at a separate project with repository at https://bitbucket.org/pablobm/ladder-api.

## Shout out to...

This app uses the following icons, sourced from the Noun Project:

  * [Back](https://thenounproject.com/term/back/26915) by Lucas Olaerts
  * [Add Friend](https://thenounproject.com/term/add-friend/138580) by Michael Martinho
  * [Trophy](https://thenounproject.com/term/trophy/412) by Edward Boatman
  * [History](https://thenounproject.com/term/history/11143/) by Ema Dimitrova
  * [More](https://thenounproject.com/term/more/37665) by Xinh Studio

## Prerequisites

To build this project, you will need [Node.JS](http://nodejs.org), [NPM](http://www.ember-cli.com) and [Ember-CLI](http://www.ember-cli.com).

## Installation

  * Fork this repository and `git clone` it
  * Change into the new directory
  * `npm install`
  * `bower install`
  * Copy `.env.example` to `.env`, and tweak at will

Although not 100% required, you probably will also want to grab a copy of the API at https://bitbucket.org/pablobm/ladder-api. Follow the instructions there.

## Running the app

To run a development server, you'll need an API. Run the API and note the host where it can be reached. Assuming you run it in development mode in your machine, it will probably listen at http://0.0.0.0:3000, and you can run the frontend like this:

  * `ember server --proxy http://0.0.0.0:3000`
  * Visit the app at http://localhost:4200

NOTE: the example above specifies that the server runs at `0.0.0.0`, and not `localhost`. This is intentional: the API runs Puma, which is less flexible about this than you may be used to.

## Running Tests

The easiest way to run tests in your machine is to do it on the browser. Once the development server is up and running, visit http://localhost:4200/tests, and they will run.

## Deploying

Deployed to Heroku using the official Heroku buildpack as seen at https://github.com/heroku/heroku-buildpack-ember-cli. The app's name is `ladder-client-ember`. If you wish to obtain access, ask Pablo (@thoughtbot.com).

## Configuration variables

Configuration variables live at `.env`, and are as follows:

  * `API_BASE_URL`: URL where the API can be found

## Further Reading / Useful Links

  * [ember.js](http://emberjs.com/)
  * [ember-cli](http://www.ember-cli.com/)
  * Development Browser Extensions
      * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
      * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

